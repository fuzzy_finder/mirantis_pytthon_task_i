#!/bin/python
# -*- coding: utf-8 -*-

import time
import sys


def tailf(file_to_read):
    with open(file_to_read, "r") as f:
        while True:
            line = f.readline()
            if line:
                yield line
            else:
                time.sleep(0.5)

if __name__ == "__main__":
    file_to_read = sys.argv[1]
    for line in tailf(file_to_read):
        print line.strip()
