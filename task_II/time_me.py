def time_me(time_func, stat_storage):
    def dec(f):
        def wrapper(*args, **kwargs):
            # computing time consuming for function
            start_time = time_func()
            retvalue = f(*args, **kwargs)
            end_time = time_func()

            time_of_processing = end_time - start_time

            # update statistic on function
            if not stat_storage.get("num_calls"):
                stat_storage["num_calls"] = 1
            else:
                stat_storage["num_calls"] += 1

            if not stat_storage.get("sum_time"):
                stat_storage["sum_time"] = time_of_processing
            else:
                stat_storage["sum_time"] += time_of_processing

            return retvalue

        return wrapper

    return dec
