cache_storage = {}


def cache_dec(f):
    def wrapper(func_param):
        if not cache_storage.get(func_param):
            cache_storage[func_param] = f(func_param)

        return cache_storage[func_param]

    return wrapper
